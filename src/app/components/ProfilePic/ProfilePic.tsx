import React from "react";
import Image from "next/image";
import profile from "../../images/profile.webp";

const ProfilePic = () => {
  return (
    <div className="rounded-full w-48 h-48 border-solid border-white border grid place-items-center">
      <Image
        src={profile}
        alt="Dylan Jones"
        className="rounded-full w-40 h-40"
      />
    </div>
  );
};

export default ProfilePic;
