import React from "react";
import ProfilePic from "../ProfilePic";

import "./Header.scss";

export type HeaderProps = {
  onAboutClicked: Function;
  onContactClicked: Function;
  className?: string;
};

function Header({ onAboutClicked, onContactClicked, className }: HeaderProps) {
  const [pageClicked, setPageClicked] = React.useState(false);

  return (
    <header className={`grid place-items-center m-4 ${className}`}>
      <ProfilePic />
      <div className="border-l h-12" />
      <div className="border-y border-white">
        <div className={`${pageClicked ? "" : "expand-height"}`}>
          <h1 className="text-4xl uppercase text-center p-4">Dylan Jones</h1>
          <p className="uppercase text-center max-w-3xl p-4">
            Senior Software Engineer - Technical Lead with 9+ years experience
            designing and developing full stack software (primarily C# and
            ASP.NET)
          </p>
        </div>
      </div>
      <div className="border-l h-12" />
      <nav className="w-fit">
        <ul className="border-solid border-white border flex justify-center rounded flex-col sm:flex-row">
          <li
            onClick={() => {
              onAboutClicked();
              setPageClicked(true);
            }}
            className="border-solid border-white border-b sm:border-b-0 sm:border-r w-32 h-12 grid place-items-center hover:bg-white/10 hover:cursor-pointer"
          >
            <button className="text-lg uppercase flex justify-center items-center w-full h-full">
              About
            </button>
          </li>
          <li className="border-solid border-white border-b sm:border-b-0 sm:border-r w-32 h-12 grid place-items-center hover:bg-white/10 hover:cursor-pointer">
            <a
              className="text-lg uppercase flex justify-center items-center w-full h-full"
              href="/Resume.pdf"
            >
              Resume
            </a>
          </li>
          <li className="border-solid border-white border-b sm:border-b-0 sm:border-r w-32 h-12 grid place-items-center hover:bg-white/10 hover:cursor-pointer">
            <a
              className="text-lg uppercase flex justify-center items-center w-full h-full"
              href="https://blog.dylanjones.dev"
            >
              Blog
            </a>
          </li>
          <li
            onClick={() => {
              onContactClicked();
              setPageClicked(true);
            }}
            className="border-solid border-white w-32 h-12 grid place-items-center hover:bg-white/10 hover:cursor-pointer"
          >
            <button className="text-lg uppercase flex justify-center items-center w-full h-full">
              Contact
            </button>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
