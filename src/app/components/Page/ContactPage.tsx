import Link from "next/link";
import React from "react";

import Page from "./Page";

export type ContactPageProps = {
  opened: boolean;
  onClose: Function;
};

function ContactPage({ opened, onClose }: ContactPageProps) {
  return (
    <Page header="Contact" opened={opened} onClose={onClose}>
      Contact me via{" "}
      <Link
        className="external"
        href="https://www.linkedin.com/in/dylan-jones-5106b51a0/"
      >
        LinkedIn
      </Link>{" "}
      or via email at{" "}
      <Link className="external" href="mailto:dylan@dylanjonesdev">
        dylan@dylanjones.dev
      </Link>
    </Page>
  );
}

export default ContactPage;
