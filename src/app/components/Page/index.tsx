export { default } from "./Page";
export { default as AboutPage } from "./AboutPage";
export { default as ContactPage } from "./ContactPage";
