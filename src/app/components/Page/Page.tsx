import React from "react";

import "./Page.scss";

export type PageProps = {
  header?: string;
  opened: boolean;
  onClose: Function;
  children?: React.ReactNode;
};

function Page({ header, opened, onClose, children }: PageProps) {
  return (
    <div className="flex justify-center">
      <div
        className={`page top-[10%] sm:top-[25%] rounded p-4 max-w-3xl w-4/6 ${
          !opened ? "hidden" : ""
        }`}
      >
        <button
          onClick={() => onClose()}
          className="text-4xl font-light mr-3 ml-auto flex items-end"
        >
          &times;
        </button>
        <h1 className="text-4xl uppercase underline underline-offset-8 font-light my-8">
          {header}
        </h1>
        <div>{children}</div>
      </div>
    </div>
  );
}

export default Page;
