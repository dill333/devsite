import React from "react";

import Page from "./Page";

export type AboutPageProps = {
  opened: boolean;
  onClose: Function;
};

function AboutPage({ opened, onClose }: AboutPageProps) {
  return (
    <Page header="About" opened={opened} onClose={onClose}>
      Senior Software Engineer - Technical Lead with 9+ years of experience
      designing and developing full stack software, primarily with a C#
      (ASP.NET) backend and various JavaScript and TypeScript frameworks for the
      frontend. Most recently learned and developed applications using React and
      TypeScript.
      <br />
      Focused on quality, security, and simplicity.
    </Page>
  );
}

export default AboutPage;
