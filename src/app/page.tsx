"use client";

import React from "react";

import Header from "./components/Header";
import { AboutPage, ContactPage } from "./components/Page";

function IndexPage() {
  const [aboutOpened, setAboutOpened] = React.useState(false);
  const [contactOpened, setContactOpened] = React.useState(false);
  const [blurredOnce, setBlurredOnce] = React.useState(false);

  const pageOpened = aboutOpened || contactOpened;

  let bgClass = "";

  if (!blurredOnce) {
    bgClass = "fadein-4s";
  } else {
    if (pageOpened) {
      bgClass = "blurin-2s";
    } else {
      bgClass = "blurout-2s";
    }
  }

  return (
    <main>
      <title>Dylan Jones</title>
      <div className={`bg ${bgClass}`} />
      <div>
        <Header
          className={`${pageOpened ? "hidden" : "fadein-2s"}`}
          onAboutClicked={() => {
            setAboutOpened(true);
            setBlurredOnce(true);
          }}
          onContactClicked={() => {
            setContactOpened(true);
            setBlurredOnce(true);
          }}
        />
      </div>
      <AboutPage opened={aboutOpened} onClose={() => setAboutOpened(false)} />
      <ContactPage
        opened={contactOpened}
        onClose={() => setContactOpened(false)}
      />
    </main>
  );
}

export default IndexPage;
